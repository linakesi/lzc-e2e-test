export interface LazycatAppMetaModel {
  name: string;
  pkgId: string;
  pkgPath: string;
  pkgHash: string;
  description: string;
  category: string;
  price: string;

  /**
   * 不支持的平台
   */
  unsupportedPlatforms: ISupportPlatform[];

  /**
   * @deprecated use (supportMobile/supportPC)
   */
  screenshotPaths: string[];

  mobileScreenshotPaths: string[];
  pcScreenshotPaths: string[];
  supportMobile: boolean;
  supportPC: boolean;
  iconPath: string;
  keywords: string;
  version: string;

  author: string;
  /** 来源 */
  source: string;

  authorId: string;
  updateId: number;
  changelog: string;
  updateDate: string;
}

export type ISupportPlatform =
  | "macos"
  | "windows"
  | "linux"
  | "android"
  | "ios";
