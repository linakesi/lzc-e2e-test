import { expect, browser } from "@wdio/globals";

const userInfo = {
  boxname: "pbkfang",
  username: "hzf",
  password: "123qaz",
};

const applist = [
  {
    host: "cloud.lazycat.app.downloader",
    id: "下载器",
    open: "true",
  },

  {
    host: "cloud.lazycat.app.photo",
    id: "懒猫相册",
    open: "true",
  },
  {
    host: "cloud.lazycat.app.video",
    id: "视频播放器",
    open: "true",
  },
  {
    host: "cloud.lazycat.app.ce",
    id: "观影助手",
  },
  {
    host: "cloud.lazycat.app.ocr",
    id: "文字识别",
  },
];

describe("步骤测试", () => {
  it("登录首页", async () => {
    await browser.url(`https://${userInfo.boxname}.heiyu.space`);
    const title = await browser.getTitle();
    await expect(title).toContain("登录懒猫微服");
    const usernameInput = await $("#username");
    await usernameInput.addValue(userInfo.username);
    const passwordInput = await $("#password");
    await passwordInput.addValue(userInfo.password);
    const submit = await $("#submit");
    await submit.click();
    const url = await browser.getUrl();
    await expect(url).toContain(`${userInfo.boxname}.heiyu.space`);
  });

  it("x", async () => {
    await browser.url(`https://appstore.${userInfo.boxname}.heiyu.space`);
    browser.setupInterceptor();
    await browser.waitUntil(
      async () =>
        await browser.execute(() => document.readyState === "complete")
    );
    for (let i = 0; i < applist.length; i++) {
      browser.expectRequest(
        "POST",
        `https://appstore.${userInfo.boxname}.heiyu.space/cloud.lazycat.apis.sys.PackageManager/Install`,
        200
      );
      const btn = await $(`#${applist[i].id}`);
      await expect(btn).toBeClickable();
      await btn.click();
      await browser.pause(1500);
      await new Promise((resolve) => {
        const timerID = setInterval(async () => {
          const requests = await browser.getRequests();
          console.log("requests", requests);
          const hasRequest = requests.find((item) =>
            item.url.includes(
              `https://appstore.${userInfo.boxname}.heiyu.space/cloud.lazycat.apis.sys.PackageManager/Install`
            )
          );
          console.log("hasRequest?.pending", hasRequest?.pending);
          if (hasRequest) {
            clearInterval(timerID);
            resolve(true);
          }
        }, 1000);
      });
    }
    await browser.pause(3000000);
  });
});
