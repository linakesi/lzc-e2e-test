import { expect, browser } from "@wdio/globals";

const userInfo = {
  boxname: "pbkfang",
  username: "hzf",
  password: "123qaz",
};

const applist = [
  {
    host: "cloud.lazycat.app.downloader",
    id: "下载器",
    open: "true",
  },

  {
    host: "cloud.lazycat.app.photo",
    id: "懒猫相册",
    open: "true",
  },
  {
    host: "cloud.lazycat.app.video",
    id: "视频播放器",
    open: "true",
  },
  {
    host: "cloud.lazycat.app.ce",
    id: "观影助手",
  },
  {
    host: "cloud.lazycat.app.ocr",
    id: "文字识别",
  },
];

export const waitForRequest = async (browser, match) =>
  new Promise((resolve) => {
    let timeout = 0;
    const step = 1000;
    const interval = setInterval(async () => {
      if (timeout <= 20000) {
        const requests = await browser.getRequests({ includePending: true });
        console.log("requests", requests);
        const hasRequest = requests.find((r) => r.url.includes(match));
        if (hasRequest) {
          clearInterval(interval);
          resolve(true);
        } else {
          timeout += 1;
        }
      } else {
        resolve(false);
        clearInterval(interval);
      }
    }, step);
  });

describe("launcher 测试", () => {
  it("登录首页", async () => {
    await browser.url(`https://${userInfo.boxname}.heiyu.space`);
    const title = await browser.getTitle();
    await expect(title).toContain("登录懒猫微服");
    const usernameInput = await $("#username");
    await usernameInput.addValue(userInfo.username);
    const passwordInput = await $("#password");
    await passwordInput.addValue(userInfo.password);
    const submit = await $("#submit");
    await submit.click();
    const url = await browser.getUrl();
    await expect(url).toContain(`${userInfo.boxname}.heiyu.space`);
  });

  it("测试安装 app", async () => {
    await browser.newWindow(`https://appstore.${userInfo.boxname}.heiyu.space`);
    await browser.waitUntil(
      () => browser.execute(() => document.readyState === "complete"),
      { timeout: 10e3 }
    );
    const appstoreTitle = await browser.getTitle();
    await expect(appstoreTitle).toContain("懒猫应用商店");

    const installApi = await browser.mock(
      "**" + "/cloud.lazycat.apis.sys.PackageManager/Install"
    );

    for (let i = 0; i < applist.length; i++) {
      const currentApp = applist[i];
      const installBtn = await $(`#${currentApp.id}`);
      await expect(installBtn).toHaveText(expect.stringContaining("安装"));
      await installBtn.click();
      // await expect(installApi).toBeRequestedWith({
      //   url: `https://appstore.${userInfo.boxname}.heiyu.space/cloud.lazycat.apis.sys.PackageManager/Install`,
      //   method: "POST",
      //   statusCode: 200,
      // });
      await installBtn.waitUntil(
        async function () {
          return (await this.getText()) === "打开";
        },
        { timeout: 120 * 1000 }
      );
      // await expect(installBtn).toHaveText(expect.stringContaining("打开"));
    }
  });

  it("测试打开 app", async () => {
    // browser.setupInterceptor();
    // browser.expectRequest(
    //   "POST",
    //   `https://appstore.${userInfo.boxname}.heiyu.space/cloud.lazycat.apis.sys.PackageManager/Uninstall`,
    //   200
    // );
    await browser.newWindow(`https://appstore.${userInfo.boxname}.heiyu.space`);
    await browser.waitUntil(
      () => browser.execute(() => document.readyState === "complete"),
      { timeout: 10e3 }
    );
    const openList = applist.filter((item) => item.open);

    for (let i = 0; i < openList.length; i++) {
      const currentApp = openList[i];
      const openBtn = await $(`#${currentApp.id}`);
      await expect(openBtn).toHaveText(expect.stringContaining("打开"));
      await openBtn.click();
    }
  });

  it("测试卸载 app", async () => {
    await browser.newWindow(`https://settings.${userInfo.boxname}.heiyu.space`);
    await browser.waitUntil(
      () => browser.execute(() => document.readyState === "complete"),
      { timeout: 10e3 }
    );

    const appManager = await $("span=应用管理");
    await expect(appManager).toHaveText("应用管理");
    await appManager.click();
    // https://settings.pbkfang.heiyu.space/cloud.lazycat.apis.sys.PackageManager/Uninstall

    for (let i = 0; i < applist.length; i++) {
      console.log("当前进行项", i);
      const currentApp = applist[i];
      const uninstallBtn = await $(`button[name="${currentApp.id}"]`);
      await expect(uninstallBtn).toBeClickable();
      await uninstallBtn.click();
      const confirmBtn = await $("div=确定");
      await expect(confirmBtn).toExist();
      await confirmBtn.click();
      await browser.pause(1000);
      await expect(confirmBtn).not.toBeDisplayed();
      // const loadingToast = await $(".lzc-loading-toast");
      // await loadingToast.waitForExist({ timeout: 3000 });
      await browser.waitUntil(
        async function () {
          return (await uninstallBtn.isExisting()) === false;
        },
        { timeout: 60 * 1000 }
      );
    }
  });
});
