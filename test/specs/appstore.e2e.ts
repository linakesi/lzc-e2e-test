import { expect, browser } from "@wdio/globals";

import { LazycatAppMetaModel } from "../type";

const userInfo = {
  boxname: "pbkfang",
  username: "hzf",
  password: "123qaz",
};

let appList: LazycatAppMetaModel[] = [];
const data = await fetch("https://appstore.lazycat.cloud/api/app/list");

appList = (await data.json()).data;

describe("测试商城所有 app 的打开情况", async () => {
  it("登录首页", async () => {
    await browser.url(`https://${userInfo.boxname}.heiyu.space`);
    const title = await browser.getTitle();
    await expect(title).toContain("登录懒猫微服");
    const usernameInput = await $("#username");
    await usernameInput.addValue(userInfo.username);
    const passwordInput = await $("#password");
    await passwordInput.addValue(userInfo.password);
    const submit = await $("#submit");
    await submit.click();
    const url = await browser.getUrl();
    await expect(url).toContain(`${userInfo.boxname}.heiyu.space`);
  });

  let appstoreWindow = "";
  it("进入商城", async () => {
    appstoreWindow = await browser.newWindow(
      `https://appstore.${userInfo.boxname}.heiyu.space`
    );
  });

  let settingWindow = "";
  for (let i = 0; i < 2; i++) {
    it(`测试${appList[i].name}`, async () => {
      const currentApp = appList[i];
      await browser.newWindow(
        `https://appstore.${userInfo.boxname}.heiyu.space/#/shop/detail/${currentApp.pkgId}`
      );
      const button = await $(`[appid="${currentApp.pkgId}"]`);
      await expect(button).toBeDisplayed();
      const buttonText = await button.getText();
      await browser.pause(3000);
      if (buttonText === "安装") {
        await expect(button).toHaveText(expect.stringContaining("安装"));
        await button.click();
        await button.waitUntil(
          async function () {
            return (await button.getText()) === "打开";
          },
          { timeout: 120 * 1000 }
        );
      }

      await browser.pause(3000);
      const domain = currentApp.pkgId.split("cloud.lazycat.app.")[1];
      await browser.closeWindow();
      await browser.switchToWindow(appstoreWindow);

      await browser.newWindow(
        `https://${domain}.${userInfo.boxname}.heiyu.space`
      );
      await browser.waitUntil(async function () {
        const title = await browser.getTitle();
        console.log("检测白屏时间 title 是否可以正确拿到", title);
        return !!title;
      });
      await browser.waitUntil(async function () {
        const title = await browser.getTitle();
        console.log("检测应用启动中的 title 是否正常", title);
        return title !== "应用正在启动中...";
      });

      await browser.pause(6000);

      await browser.closeWindow();
      await browser.switchToWindow(appstoreWindow);
      // 获取当前窗口列表，
      // 如果 setting 没有打开，则 newWindow 打开,
      // 如果已经打开，则切换窗口至 Setting

      if (!settingWindow) {
        settingWindow = await browser.newWindow(
          `https://settings.${userInfo.boxname}.heiyu.space`
        );
      } else {
        await browser.switchToWindow(settingWindow);
      }

      const indexTab = await $("span=懒猫微服");
      await expect(indexTab).toHaveText("懒猫微服");
      await indexTab.click();
      await browser.pause(1000);
      const appManager = await $("span=应用管理");
      await expect(appManager).toHaveText("应用管理");
      await appManager.click();

      const uninstallBtn = await $(`[id="${currentApp.pkgId}"]`);
      await expect(uninstallBtn).toBeClickable();
      await uninstallBtn.click();
      const confirmBtn = await $("div=确定");
      await expect(confirmBtn).toExist();
      await confirmBtn.click();
      await browser.pause(1000);
      await expect(confirmBtn).not.toBeDisplayed();
      await browser.waitUntil(
        async function () {
          return (await uninstallBtn.isExisting()) === false;
        },
        { timeout: 120 * 1000 }
      );
      await browser.pause(3000);
    });
  }
});
