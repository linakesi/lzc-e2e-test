import { expect, browser } from "@wdio/globals";

const userInfo = {
  boxname: "pbkfang",
  username: "hzf",
  password: "123qaz",
};

const applist = [
  {
    host: "cloud.lazycat.app.downloader",
    id: "下载器",
    open: true,
  },

  {
    host: "cloud.lazycat.app.photo",
    id: "懒猫相册",
    open: true,
  },
  {
    host: "cloud.lazycat.app.video",
    id: "视频播放器",
    open: true,
  },
  {
    host: "cloud.lazycat.app.ce",
    id: "观影助手",
  },
  {
    host: "cloud.lazycat.app.ocr",
    id: "文字识别",
  },
];

describe("软件测试", () => {
  it("登录首页", async () => {
    await browser.url(`https://${userInfo.boxname}.heiyu.space`);
    const title = await browser.getTitle();
    await expect(title).toContain("登录懒猫微服");
    const usernameInput = await $("#username");
    await usernameInput.addValue(userInfo.username);
    const passwordInput = await $("#password");
    await passwordInput.addValue(userInfo.password);
    const submit = await $("#submit");
    await submit.click();
    const url = await browser.getUrl();
    await expect(url).toContain(`${userInfo.boxname}.heiyu.space`);
  });

  let appstoreWindow = "";
  let settingWindow = "";
  for (let i = 0; i < applist.length; i++) {
    it(`测试${applist[i].id}`, async () => {
      //1. 查看准备测试的 app
      const currentApp = applist[i];
      const windowList = await browser.getWindowHandles();
      if (!windowList.find((item) => item === appstoreWindow)) {
        appstoreWindow = await browser.newWindow(
          `https://appstore.${userInfo.boxname}.heiyu.space`
        );
      } else {
        await browser.switchToWindow(appstoreWindow);
      }
      const appstoreTitle = await browser.getTitle();
      await expect(appstoreTitle).toContain("懒猫应用商店");
      const installBtn = await $(`#${currentApp.id}`);
      await expect(installBtn).toHaveText(expect.stringContaining("安装"));
      await installBtn.click();
      await installBtn.waitUntil(
        async function () {
          return (await installBtn.getText()) === "打开";
        },
        { timeout: 120 * 1000 }
      );
      await browser.pause(3000);

      //2.打开新安装的网页
      if (currentApp.open) {
        const openBtn = await $(`#${currentApp.id}`);
        await openBtn.click();
        await browser.pause(5000);
      }

      //3. 测试卸载
      if (!windowList.find((item) => item === settingWindow)) {
        settingWindow = await browser.newWindow(
          `https://settings.${userInfo.boxname}.heiyu.space`
        );
        await browser.waitUntil(
          () => browser.execute(() => document.readyState === "complete"),
          { timeout: 10e3 }
        );
      } else {
        await browser.switchToWindow(settingWindow);
      }

      const indexTab = await $("span=懒猫微服");
      await expect(indexTab).toHaveText("懒猫微服");
      await indexTab.click();

      const appManager = await $("span=应用管理");
      await expect(appManager).toHaveText("应用管理");
      await appManager.click();

      const uninstallBtn = await $(`button[name="${currentApp.id}"]`);
      await expect(uninstallBtn).toBeClickable();
      await uninstallBtn.click();
      const confirmBtn = await $("div=确定");
      await expect(confirmBtn).toExist();
      await confirmBtn.click();
      await browser.pause(1000);
      await expect(confirmBtn).not.toBeDisplayed();
      // const loadingToast = await $(".lzc-loading-toast");
      // await loadingToast.waitForExist({ timeout: 3000 });
      await browser.waitUntil(
        async function () {
          return (await uninstallBtn.isExisting()) === false;
        },
        { timeout: 60 * 1000 }
      );
      await browser.pause(3000);
    });
  }
});
