import { expect, browser } from "@wdio/globals";
import json from "../fixture/errorlist.json" assert { type: "json" };

const userInfo = {
  boxname: "pbkfang",
  username: "hzf",
  password: "123qaz",
};

describe("测试 error app", () => {
  it("登录首页", async () => {
    await browser.url(`https://${userInfo.boxname}.heiyu.space`);
    const title = await browser.getTitle();
    await expect(title).toContain("登录懒猫微服");
    const usernameInput = await $("#username");
    await usernameInput.addValue(userInfo.username);
    const passwordInput = await $("#password");
    await passwordInput.addValue(userInfo.password);
    const submit = await $("#submit");
    await submit.click();
    const url = await browser.getUrl();
    await expect(url).toContain(`${userInfo.boxname}.heiyu.space`);
  });

  let appstoreWindow = "";
  let settingWindow = "";

  it("进入商城", async () => {
    appstoreWindow = await browser.newWindow(
      `https://appstore.${userInfo.boxname}.heiyu.space`
    );
  });

  for (let i = 0; i < json.length; i++) {
    describe(`测试 ${json[i].name} 整体功能`, () => {
      it(`测试 ${json[i].name} 的安装`, async () => {
        const currentApp = json[i];
        await browser.newWindow(
          `https://appstore.${userInfo.boxname}.heiyu.space/#/shop/detail/${currentApp.pkgId}`
        );
        const button = await $(`[appid="${currentApp.pkgId}"]`);
        await expect(button).toBeDisplayed();
        const buttonText = await button.getText();
        await browser.pause(3000);
        if (buttonText === "安装") {
          await expect(button).toHaveText(expect.stringContaining("安装"));

          await browser.takeScreenshot();
          await button.click();
          await button.waitUntil(
            async function () {
              return (await button.getText()) === "打开";
            },
            { timeout: 120 * 1000 }
          );
        }
        await browser.takeScreenshot();
      });

      it(`测试 ${json[i].name} 打开`, async () => {
        const currentApp = json[i];
        const button = await $(`[appid="${currentApp.pkgId}"]`);
        await expect(button).toHaveText(expect.stringContaining("打开"));
        await browser.pause(3000);
        const domain = currentApp.pkgId.split("cloud.lazycat.app.")[1];
        await browser.closeWindow();
        await browser.switchToWindow(appstoreWindow);
        await browser.newWindow(
          `https://${domain}.${userInfo.boxname}.heiyu.space`
        );
        await browser.waitUntil(async function () {
          const title = await browser.getTitle();
          console.log("检测白屏时间 title 是否可以正确拿到", title);
          return !!title;
        });
        await browser.takeScreenshot();
        await browser.waitUntil(async function () {
          const title = await browser.getTitle();
          console.log("检测应用启动中的 title 是否正常", title);
          return title !== "应用正在启动中...";
        });
        await browser.takeScreenshot();
        await browser.pause(3000);
        await browser.closeWindow();
        await browser.switchToWindow(appstoreWindow);
      });

      it(`测试 ${json[i].name} 卸载`, async () => {
        const currentApp = json[i];
        if (settingWindow) {
          await browser.switchToWindow(settingWindow);
        } else {
          settingWindow = await browser.newWindow(
            `https://settings.${userInfo.boxname}.heiyu.space`
          );
        }
        const indexTab = await $("span=懒猫微服");
        await expect(indexTab).toHaveText("懒猫微服");
        await indexTab.click();
        await browser.pause(1000);
        const appManager = await $("span=应用管理");
        await expect(appManager).toHaveText("应用管理");
        await appManager.click();

        const uninstallBtn = await $(`[id="${currentApp.pkgId}"]`);
        await expect(uninstallBtn).toBeClickable();
        await uninstallBtn.click();
        const confirmBtn = await $("div=确定");
        await browser.takeScreenshot();
        await expect(confirmBtn).toExist();
        await confirmBtn.click();
        await browser.pause(1000);
        await expect(confirmBtn).not.toBeDisplayed();
        await browser.waitUntil(
          async function () {
            return (await uninstallBtn.isExisting()) === false;
          },
          { timeout: 120 * 1000 }
        );
        await browser.takeScreenshot();
        await browser.pause(3000);
      });
    });
  }
});
